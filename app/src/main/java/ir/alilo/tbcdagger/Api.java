package ir.alilo.tbcdagger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Api {
    @Inject ConnectionHelper mConnectionHelper;

    @Inject
    public Api() {

    }

    public String getMe() {
        try {
            return mConnectionHelper.makeGet("getMe");
        } catch (IOException e) {
            return "IO exception occurred. " + e.getMessage();
        }
    }

    public String sendMessage(int chatId, String text) {
        try {
            return mConnectionHelper.makePost("sendMessage",
                    new JSONObject()
                            .put("chat_id", chatId)
                            .put("text", text)
            );
        } catch (JSONException e) {
            return "Json exception occurred. " + e.getMessage();
        } catch (IOException e) {
            return "IO exception occurred. " + e.getMessage();
        }
    }
}
