package ir.alilo.tbcdagger;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ConnectionModule {
    private final String mBotToken;

    public ConnectionModule(String botToken) {
        mBotToken = botToken;
    }

    @Provides
    @Singleton
    public ConnectionHelper provideConnectionHelper() {
        return new ConnectionHelper(mBotToken);
    }
}
