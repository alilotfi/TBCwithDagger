package ir.alilo.tbcdagger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class ConnectionHelper {

    private static final MediaType JSON_MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");
    private static final String TELEGRAM_URL = "https://api.telegram.org/";
    private final String BASE_URL;
    private OkHttpClient mClient;

    @Inject
    public ConnectionHelper(String botToken) {
        mClient = new OkHttpClient();
        BASE_URL = TELEGRAM_URL + "bot" + botToken + "/";
    }

    public String makeGet(String methodName) throws IOException {
        Request request = new Request.Builder()
                .url(BASE_URL + methodName)
                .build();

        Response response = mClient.newCall(request).execute();
        ResponseBody body = response.body();
        if (body == null) {
            return null;
        }

        try {
            return beautifyResponse(body.string());
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    public String makePost(String methodName, JSONObject data) throws IOException {
        RequestBody requestBody = RequestBody.create(JSON_MEDIA_TYPE, data.toString());
        Request request = new Request.Builder()
                .url(BASE_URL + methodName)
                .post(requestBody)
                .build();

        Response response = mClient.newCall(request).execute();
        ResponseBody responseBody = response.body();
        if (responseBody == null) {
            return null;
        }
        try {
            return beautifyResponse(responseBody.string());
        } catch (JSONException e) {
            throw new IOException(e);
        }
    }

    private String beautifyResponse(String response) throws JSONException {
        JSONObject jsonResponse;
        String jsonString;
        jsonResponse = new JSONObject(response);
        jsonString = jsonResponse.toString(4);
        return jsonString;
    }
}
