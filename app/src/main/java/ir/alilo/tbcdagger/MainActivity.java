package ir.alilo.tbcdagger;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.inject.Inject;
import javax.inject.Singleton;

public class MainActivity extends AppCompatActivity {
    @Inject Api mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((TBCApplication) getApplication()).mComponent.inject(this);

        Button btnGetMe = (Button) findViewById(R.id.btnMainGetMe);
        if (btnGetMe != null) {
            btnGetMe.setOnClickListener(new OnActionListener() {
                @Override
                public String run() {
                    return mApi.getMe();
                }
            });
        }

        Button btnSendMessage = (Button) findViewById(R.id.btnMainSendMessage);
        if (btnSendMessage != null) {
            btnSendMessage.setOnClickListener(new OnActionListener() {
                @Override
                public String run() {
                    return mApi.sendMessage(112109338, "Test message from Telegram bot client.");
                }
            });
        }

    }

    private abstract class OnActionListener implements View.OnClickListener {
        private final TextView mTvResult = (TextView) findViewById(R.id.tvMainResult);
        @Override
        public void onClick(View v) {
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    return run();
                }

                @Override
                protected void onPostExecute(String s) {
                    mTvResult.setText(s);
                }
            }.execute();
        }

        public abstract String run();
    }
}
