package ir.alilo.tbcdagger;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Component;

public class TBCApplication extends Application {

    private static final String BOT_TOKEN = "170735826:AAFSiX8DZRmCvq0vV9WadEScpxvdd4akLpI";

    @Singleton
    @Component(modules = ConnectionModule.class)
    public interface ApplicationComponent {
        void inject(MainActivity mainActivity);
    }

    public ApplicationComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerTBCApplication_ApplicationComponent.builder()
                .connectionModule(new ConnectionModule(BOT_TOKEN))
                .build();
    }
}
